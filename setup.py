import setuptools

setuptools.setup(
    name="py-flasher-hbber",
    version="0.0.1",
    author="zhanghao",
    author_email="zhanghao190@mails.ucas.ac.cn",
    description="一个用来给TI mmWave芯片烧写固件的python库，可以用来做空中升级(OTA)。",
    long_description="long_description",
    long_description_content_type="text/markdown",
    url="https://gitee.com/haobaobao/py-flasher",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
