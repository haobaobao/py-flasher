## Py-Flasher

一个用来给TI mmWave芯片烧写固件的python库，可以用来做空中升级(OTA)。

### 安装

* 方法一：直接将文件夹放到工程目录下
* 方法二：pip

### 使用

1. 检查主机和雷达器件之间的物理连线：UART(TX, RX, GND)、SOP2、 RST。

2. 执行`python3 py-flasher.py download '/dev/ttyS1' './xxx.bin'`

3. 等待执行完毕

   ```bash
   PS F:\Synology\SynologyDrive\projs\Python_space\py-flasher> python .\pyflasher.py download '.\resources\xwr68xx_mmw_demo.bin' --device_name=debug
   F:\Synology\SynologyDrive\projs\Python_space\py-flasher
   设置IWR6843-debug为flashing模式...
   开始下载".\resources\xwr68xx_mmw_demo.bin"到IWR6843, 固件大小:510KB, 预计下载耗时36秒
   100%|██████████████████████████████████████████████████████████████████████████████| 2177/2177 [00:52<00:00, 41.23it/s]
   固件".\resources\xwr68xx_mmw_demo.bin"下载成功, 耗时:58.807秒, 请将SOP2拉低，复位芯片以启动新的程序！
   PS F:\Synology\SynologyDrive\projs\Python_space\py-flasher>
   ```

   

### 作为库使用
TODO

## 程序设计思路

我们将串口下载程序封装为一个库，外面套一层shell壳子，可以直接命令行使用，亦可以集成到整个项目代码中。为了可扩展，我们将设计一个基类，实现串口的管理，定义好行为函数，对于具体的芯片，我们实现基类所定义的接口即可。

> 我们将这个工具命名为：**py-flasher**

涉及到的核心库：

* **pyserial**：牛🐂逼😮炸💣天🌤的python库，推荐直接看[官方文档](https://pythonhosted.org/pyserial/pyserial_api.html)，很详细。
* **rich**：Rich是一个Python库，用于向终端编写富文本(带有颜色和样式)，并用于显示高级内容，如表格、标记和语法突出显示代码。[文档](https://rich.readthedocs.io/en/latest/introduction.html)  [掘金](https://juejin.cn/post/6844904179333332999)
* **Typer**：Typer是一个用于构建CLI应用程序的库，用户会喜欢使用它，开发人员也会喜欢创建它。 [文档](https://typer.tiangolo.com/)

见示例代码：demo


## 主要参考资料

TI Application Report- SWRA627：[IWR6843 BootLoader Flow](https://www.ti.com.cn/cn/lit/an/swra627/swra627.pdf)

CSDN：[UART的break信号](https://blog.csdn.net/Colorful_lights/article/details/80911225?utm_source=blogxgwz8)
