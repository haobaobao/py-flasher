#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2021/4/9 18:58
# @Author  : Hbber
# @Version : 0.0.0
# @File    : my_logging.py
# @Software: PyCharm
# @Org     : AirCas
# @Describe: 

# @Last Modify Time          @Version        @Description
# --------------------       --------        -----------
# 2021/4/9 18:58            0.0.1           None

import logging
import os
from logging.handlers import RotatingFileHandler

LOG = os.environ.get('LOG')
PKG_PATH = os.path.normpath(os.path.dirname(os.path.abspath(__file__)))
print(PKG_PATH)


def getLogger(name):
    """
    作用同标准模块 logging.getLogger(name)

    :returns: logger
    """
    LEVEL = logging.DEBUG
    if LOG is None or LOG == 0:
        LEVEL = logging.INFO

    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(filename)s - %(funcName)s - line %(lineno)s - %(levelname)s - %(message)s')

    logger = logging.getLogger(name)
    logger.setLevel(LEVEL)

    # FileHandler
    file_handler = RotatingFileHandler(os.path.join(PKG_PATH, '../log/py-flasher.log'), maxBytes=1024 * 1024, backupCount=5)
    file_handler.setLevel(level=LEVEL)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    return logger
