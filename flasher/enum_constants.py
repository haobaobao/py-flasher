#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2021/4/8 21:01
# @Author  : Hbber
# @Version : 0.0.0
# @File    : enum_constants.py
# @Software: PyCharm
# @Org     : AirCas
# @Describe: 

# @Last Modify Time          @Version        @Description
# --------------------       --------        -----------
# 2021/4/8 21:01            0.0.1           None

import enum

SYNC_HEADER = 0xAA
MAX_DATA_CHUNK_SIZE = 240
BAUD_RATE = 115200
IWR6843_ISK_PORT = '/dev/ttyS1'
IWR6843_ODS_PORT = '/dev/ttyS2'


class CMD(enum.Enum):
    """
    6843支持的命令枚举
    """
    # The device responds with ACK
    PING = 0x20
    # Command that gives details about the type of
    # file being downloaded
    OPEN_FILE = 0x21
    # Command that gives the content of the file to
    # write to SFLASH
    WRITE_FILE_TO_SFLASH = 0x24
    # Command that gives the content of the file
    # and the file is directly written to RAM
    WRITE_FILE_TO_RAM = 0x26
    # Command that indicates the end-of-file
    # download
    CLOSE_FILE = 0x22
    # Command that requests the status of the
    # previous command. The device responds with
    # the status of the previous command issued.
    GET_STATUS = 0x23
    # Command to erase the contents of the
    # SFALSH
    ERASE_DEVICE = 0x28
    # Command that requests the version of the
    # ROM. Device responds with the version
    # information.
    GET_VERSION = 0x2F
    # Response from the device
    ACK_RESPONSE = 0xCC

    @staticmethod
    def get_name():
        return "CMD"


class FILE_TYPE(enum.Enum):
    META_IMG_1 = 4
    META_IMG_2 = 5
    META_IMG_3 = 6
    META_IMG_4 = 7

    @staticmethod
    def get_name():
        return "FILE_TYPE"


class STORAGE_TYPE(enum.Enum):
    SFLASH = 2
    SRAM = 4

    @staticmethod
    def get_name():
        return "STORAGE_TYPE"


class STATUS_RESPONSE(enum.Enum):
    """
    The STATUS RESPONSE returned from the device is the bootloader error status based on the last
    actionable command executed. Actionable commands include OPEN, WRITE TO FLASH, CLOSE, and
    ERASE. Status commands like PING, GET STATUS, and GET VERSION do not affect the error status
    reported in the STATUS RESPONSE. The possible returned STATUS values are as follows.
        0x00 = INITIAL_STATUS (before any actionable commands are issued)
        0x40 = SUCCESS
        0x4B = ACCESS_IN_PROGRESS
    Any RESERVED fields in commands sent from the host should be set to 0x0.
    """
    INITIAL_STATUS = 0x00
    SUCCESS = 0x40
    ACCESS_IN_PROGRESS = 0x4B


class DEVICE(enum.Enum):
    IWR6843 = 0,
    AWR1642 = 1
